# Cut DIPAS

DIPAS verbindet das Hamburger Online-Beteiligungstool mit digitalen Planungstischen zu einem integrierten digitalen System zur Bürgerbeteiligung. Mit DIPAS können Bürgerinnen und Bürger von zu Hause aus, mobil oder in Veranstaltungen digitale Karten, Luftbilder, Pläne, 3D Modelle und Geodaten abrufen und ein genau lokalisiertes Feedback zu Planungsvorhaben geben.


## Code Basis

Die Code Basis von DIPAS befindet sich in einem anderen Open Code Repository. Dabei ist DIPAS modular aufgebaut, d. h. es gibt eine Kernkomponente (DIPAS Core) und weitere Komponenten, die bei Bedarf das Digitale Partzipationssystem (DIPAS) erweitern. Die Kernkomponente und die Erweiterungen finden sich hier:

- [DIPAS Core](https://gitlab.opencode.de/dipas/dipas-core/dipas-core)
- [DIPAS Navigator](https://gitlab.opencode.de/dipas/navigator/dipas-navigator)
- [DIPAS Masterportal Addons](https://gitlab.opencode.de/dipas/dipas-masterportal-addons/dipas-masterportal-addons)

